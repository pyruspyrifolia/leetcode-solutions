class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        res = ""

        for i in range(len(strs[0])):
            for s in strs:
                if i == len(s) or s[i] != strs[0][i]:
                    return res
            res += strs[0][i]

        return res



# Lesson: can add string to empty string
# line 7 i == len(s) is when i is out of bounds
#


class Solution:
    def longestCommonPrefix(self, v: List[str]) -> str:
        ans=""
        v=sorted(v)
        first=v[0]
        last=v[-1]
        for i in range(min(len(first),len(last))):
            if(first[i]!=last[i]):
                return ans
            ans+=first[i]
        return ans


#Initialize an empty string and to store the common prefix.
#Sort the input list v lexicographically. This step is necessary because the common prefix should be common to all the strings, so we need to find the common prefix of the first and last string in the sorted list.
#Iterate through the characters of the first and last string in the sorted list, stopping at the length of the shorter string.
#If the current character of the first string is not equal to the current character of the last string, return the common prefix found so far.
#Otherwise, append the current character to the ans string.
#Return the ans string containing the longest common prefix.
#Note that the code assumes that the input list v is non-empty, and that all the strings in v have at least one character. If either of these assumptions is not true, the code may fail.
