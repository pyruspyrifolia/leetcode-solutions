class Solution:
    def merge(self, nums1: List[int], m: int, nums2: List[int], n: int) -> None:
        """
        Do not return anything, modify nums1 in-place instead.
        """
        for num in range(n):
            nums1[(m+n)-(num+1)] = nums2[num]

        nums1.sort()


# merge two sorted arrays, basically add elements of second array to first, then sort
