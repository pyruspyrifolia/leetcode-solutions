class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        if len(s) != len(t):
            return False

        resS, resT = {}, {}

        for c in range(len(s)):
            resS[s[c]] = 1 + resS.get(s[c], 0)
            resT[t[c]] = 1 + resT.get(t[c], 0)
        for i in resS:
            if resS[i] != resT.get(i, 0):
                return False

        return True


#First Solution, make two hashmaps and compare them. The .get method is to avoid and key errors in Python, so it
