class Solution:
    def plusOne(self, digits: List[int]) -> List[int]:
        numberString = ""
        for dig in digits:
            numberString += str(dig)
        number = int(numberString)
        newNum = number + 1
        resultList = []
        for num in str(newNum):
            resultList.append(int(num))
        return resultList
