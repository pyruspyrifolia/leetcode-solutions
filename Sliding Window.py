#sliding window example
class Solution:
    def sliding_window(nums):

        left, right = 0, 0
        count_of_zeroes = 0
        global_max = 0

        while right < len(nums):

            if nums[right] == 0:
                count_of_zeroes += 1

            while count_of_zeroes == 2:
                global_max = max(global_max, right - left)

                if nums[left] == 0:
                    count_of_zeroes -= 1
                left += 1

            right += 1

        if count_of_zeroes < 2:
            global_max = max(global_max, right-left)

        return global_max
