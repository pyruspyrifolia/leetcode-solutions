class Node:

    def __init__(self, value):
        self.value = value
        self.link = link

    def __str__(self):

        return ("Node { value: '" + str(self.value)
                + "'link: " + str(self.link) + "}")


link = None
p = Node("paper")
r = Node("rock")
s = Node("scissors")
p.link = r
print(p)
