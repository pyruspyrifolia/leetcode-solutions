class Solution:
    def isValid(self, s: str) -> bool:
        stack = []
        char_map = {
            ")" : "(",
            "]" : "[",
            "}" : "{"
        }

        if len(s) % 2 != 0:
            return False

        for char in s:
            if char in char_map:
                if stack and stack[-1] == char_map[char]:
                    #if stack is to check stack is not empty, stack - 1 is the last element added to stack
                    # we have to make sure that parentheses is = to the closing parenthese that is char
                    stack.pop()
                else:
                    return False
            else:
                stack.append(char)

        return True if not stack else False
        # if not stack is checking that stack is completely empty
        # this solution efficiency is O(n) because going through each element once in the string
