class Solution:
    def setZeroes(self, matrix: List[List[int]]) -> None:
        """
        Do not return anything, modify matrix in-place instead.
        """
        ind = []
        for mat in matrix:
            for i, num in enumerate(mat):
                if num == 0:
                    ind.append(i)


        for mat in matrix:
            for i, num in enumerate(mat):
                if 0 in mat:
                    mat[i] = 0
            for i in ind:
                mat[i] = 0


k = []
        for i in range (len(matrix)):
            for j in range (len(matrix[0])):
                if matrix[i][j] == 0:
                    k.append((i,j))

        for i,j in k:
            matrix[i] = [0]*len(matrix[i])
            for i in range(len(matrix)):
                matrix[i][j] = 0
