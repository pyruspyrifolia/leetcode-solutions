class Solution:
    def twoSum(self, numbers: List[int], target: int) -> List[int]:
        nums2index = {}

        for i, num in enumerate(numbers):
            complement = target - num
            if complement in nums2index:
                return [nums2index[complement] + 1, i+1]
            nums2index[num] = i



# Same solution as two sum, problem 1 in leetcode
# But the solution wants you to add the index value + 1, so line 8 has been modified

class Solution:
    def twoSum(self, numbers: List[int], target: int) -> List[int]:
        left = 0
        right = len(numbers) - 1

        while left < right:
            total = numbers[left] + numbers[right]

            if total == target:
                return [left + 1, right + 1]
            elif total > target:
                right -= 1
            else:
                left += 1

# Different solution, using two pointer method
